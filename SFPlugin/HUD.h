class HUD
{
	bool bMC_HUD;
	byte HeartAnim;
	byte ArmorAnim;
	DWORD dwHeartAnim_time;
	DWORD dwArmorAnim_time;
	float fHealth;
	float fArmor;

	byte BACKUP_DrawHud[6];

	stTextureInfo *pWeaponTexture[47]; //0~46
	stTextureInfo *pSlotsTexture;
	stTextureInfo *pSelectTexture;

	stTextureInfo *p0hpTexture;
	stTextureInfo *p5hpTexture;
	stTextureInfo *p10hpTexture;

	stTextureInfo *p0apTexture;
	stTextureInfo *p5apTexture;
	stTextureInfo *p10apTexture;

	stTextureInfo *pPowerTexture;
	stTextureInfo *pPowerHalfTexture;
	stTextureInfo *pOxygenTexture;
	stTextureInfo *pOxygenDetailTexture;

	D3DCOLOR DynamycColor(float percent);
public:
	HUD();
	void Render();
	void SetHudState(bool &state);
	bool GetHudState();
	void ApplyDamage();
	~HUD();
};

extern HUD *hud;
