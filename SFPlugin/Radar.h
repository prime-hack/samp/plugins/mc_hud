#define _RadarOffset 541.21293800539083557951482479782
class Radar
{
	byte mode = 0;
	bool border;
	byte BACKUP_RadarPosition_X[6];

	byte BACKUP_RadarPosition_X_Border1[6];
	byte BACKUP_RadarPosition_X_Border2[6];
	byte BACKUP_RadarPosition_X_Border3[6];
	byte BACKUP_RadarPosition_X_Border4[6];

	byte BACKUP_HightBarPosition_X[6];
	byte BACKUP_HightBarPosition_X_separator[6];
	byte BACKUP_FlyHudPosition_X[6];
public:
	Radar();
	void CalcRadarPosition();
	void SetMode(byte mode);
	byte GetMode();
	void SetBorder(bool &Border);
	bool GetBorder();
	~Radar();
};

extern Radar *radar;

float GetRadarX(float &SX, float &SY, float &RW);
bool DrawRadarBorder();

