#include "main.h"
bool file_exist(char* file_name)
{
	WIN32_FIND_DATAA FindFileData;
	HANDLE hFindIni;

	hFindIni = FindFirstFileA(file_name, &FindFileData);
	if (hFindIni != INVALID_HANDLE_VALUE)
	{
		FindClose(hFindIni);
		return true;
	}
	FindClose(hFindIni);
	return false;
}