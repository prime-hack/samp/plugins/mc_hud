#include "main.h"

Chat::Chat()
{
	dwSAMP_Addr = SF->getSAMP()->getSAMPAddr();
	dwChatInputBox_Y = SF->getSAMP()->getChat()->dwChatboxOffset;

	memcpy_safe(BKRET_CHATINPUTADJUST_Y, (void*)(dwSAMP_Addr + SAMP_PATCH_CHATINPUTADJUST_Y), 6);
	memcpy_safe(BKRET_ChatPos, (void*)(dwSAMP_Addr + SAMP_PATCH_CHATPOS_Y), 4);

	bClassic = true;
	bMiniChat = true;
}

void Chat::SetMode(bool classic)
{
	bClassic = classic;
	SF->getSAMP()->getChat()->dwChatboxOffset = dwChatInputBox_Y;

	SF->getSAMP()->getChat()->SetDisplayMode(1);
	if (!bClassic)
	{
		memset_safe((void*)(dwSAMP_Addr + SAMP_PATCH_CHATINPUTADJUST_Y), 0x90, 6);
		memset_safe((void*)(dwSAMP_Addr + SAMP_PATCH_CHATPOS_Y), 0x90, 4);
	}
	else
	{
		memcpy_safe((void*)(dwSAMP_Addr + SAMP_PATCH_CHATINPUTADJUST_Y), BKRET_CHATINPUTADJUST_Y, 6);
		memcpy_safe((void*)(dwSAMP_Addr + SAMP_PATCH_CHATPOS_Y), BKRET_ChatPos, 4);
	}
	SF->getSAMP()->getChat()->SetDisplayMode(2);
}

bool Chat::GetMode()
{
	return bClassic;
}

void Chat::SetMini(bool &MiniChat)
{
	bMiniChat = MiniChat;
}

bool Chat::GetMini()
{
	return bMiniChat;
}

void Chat::RenderChat()
{
	if (bClassic) return;

	if (!SF->getSAMP()->getChat()->iChatWindowMode)
		return;

	if (SF->getGame()->isKeyDown(VK_F5))
		return;

	if (SF->getGame()->isKeyDown(VK_F10))
		return;

	char Text[192];
	int Y = (*(int*)0x00C9C044 * 0.75) - SF->getSAMP()->getChat()->pagesize*pFont->DrawHeight();
	int RenderFrom = SF->getSAMP()->getChat()->m_nPrevScrollBarPosition;
	byte AlphaColor = 0xFF;
	byte ARGB[4];

	if (!SF->getSAMP()->getInput()->iInputEnabled && bMiniChat)
	{
		RenderFrom += SF->getSAMP()->getChat()->pagesize / 2;
		AlphaColor = 0xE0;
	}

	for (int i = RenderFrom; i < (SF->getSAMP()->getChat()->m_nPrevScrollBarPosition + SF->getSAMP()->getChat()->pagesize); i++)
	{
		SF->getSAMP()->getChat()->dwChatboxOffset = Y + (SF->getSAMP()->getChat()->pagesize + 1) * pFont->DrawHeight();
		if (SF->getSAMP()->getChat()->chatEntry[i].clPrefixColor == 0)
			SF->getSAMP()->getChat()->chatEntry[i].clPrefixColor = 0xFFFFFF;
		if (SF->getSAMP()->getChat()->chatEntry[i].clTextColor == 0)
			SF->getSAMP()->getChat()->chatEntry[i].clTextColor = 0x283848;
		SF->getRender()->ARGB_To_A_R_G_B(SF->getSAMP()->getChat()->chatEntry[i].clTextColor, ARGB[0], ARGB[1], ARGB[2], ARGB[3]);

		if (strlen(SF->getSAMP()->getChat()->chatEntry[i].szPrefix) > 0)
		{
			sprintf(Text, "%s {%x}%s\x00", SF->getSAMP()->getChat()->chatEntry[i].szPrefix, D3DCOLOR_ARGB(AlphaColor, ARGB[1], ARGB[2], ARGB[3]), SF->getSAMP()->getChat()->chatEntry[i].szText);
			SF->getRender()->ARGB_To_A_R_G_B(SF->getSAMP()->getChat()->chatEntry[i].clPrefixColor, ARGB[0], ARGB[1], ARGB[2], ARGB[3]);

			if (SF->getSAMP()->getChat()->iChatWindowMode == 1)
				pFont->Print(Text, D3DCOLOR_ARGB(AlphaColor, ARGB[1], ARGB[2], ARGB[3]), 45, Y + (pFont->DrawHeight() * (i - SF->getSAMP()->getChat()->m_nPrevScrollBarPosition)));
			else pFont->PrintShadow(Text, D3DCOLOR_ARGB(AlphaColor, ARGB[1], ARGB[2], ARGB[3]), 45, Y + (pFont->DrawHeight() * (i - SF->getSAMP()->getChat()->m_nPrevScrollBarPosition)));
		}
		else
		{
			if (SF->getSAMP()->getChat()->iChatWindowMode == 1)
				pFont->Print(SF->getSAMP()->getChat()->chatEntry[i].szText, D3DCOLOR_ARGB(AlphaColor, ARGB[1], ARGB[2], ARGB[3]), 45, Y + (pFont->DrawHeight() * (i - SF->getSAMP()->getChat()->m_nPrevScrollBarPosition)));
			else pFont->PrintShadow(SF->getSAMP()->getChat()->chatEntry[i].szText, D3DCOLOR_ARGB(AlphaColor, ARGB[1], ARGB[2], ARGB[3]), 45, Y + (pFont->DrawHeight() * (i - SF->getSAMP()->getChat()->m_nPrevScrollBarPosition)));
		}
	}
}

Chat::~Chat()
{
	SF->getSAMP()->getChat()->dwChatboxOffset = dwChatInputBox_Y;

	SF->getSAMP()->getChat()->SetDisplayMode(1);
	memcpy_safe((void*)(dwSAMP_Addr + SAMP_PATCH_CHATINPUTADJUST_Y), BKRET_CHATINPUTADJUST_Y, 6);
	memcpy_safe((void*)(dwSAMP_Addr + SAMP_PATCH_CHATPOS_Y), BKRET_ChatPos, 4);
	SF->getSAMP()->getChat()->SetDisplayMode(2);
}
