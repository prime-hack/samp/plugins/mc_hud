#include "main.h"
float _radar_x;
DWORD HKRET_RadarPosition_X = 0x005834D8;
DWORD HKRET_RadarPosition_X_Border1 = 0x0058A79F;
DWORD HKRET_RadarPosition_X_Border2 = 0x0058A83A;
DWORD HKRET_RadarPosition_X_Border3 = 0x0058A8ED;
DWORD HKRET_RadarPosition_X_Border4 = 0x0058A98E;
DWORD HKRET_HightBarPosition_X = 0x0058A5E6;
DWORD HKRET_HightBarPosition_X_separator = 0x0058A6EA;
DWORD HKRET_FlyHudPosition_X = 0x0058A46D;

void __declspec (naked) HOOK_RadarPosition_X(void) //005834D2
{
	_asm pushad
	radar->CalcRadarPosition();

	__asm
	{
		popad
		fmul _radar_x
		jmp HKRET_RadarPosition_X
	}
}

void __declspec (naked) HOOK_RadarPosition_X_Border1(void) //0058A799
{
	_asm pushad
	if (DrawRadarBorder())
		_asm fld _radar_x

	__asm
	{
		popad
		jmp HKRET_RadarPosition_X_Border1
	}
}

void __declspec (naked) HOOK_RadarPosition_X_Border2(void) //0058A834
{
	_asm pushad
	if (DrawRadarBorder())
		_asm fld _radar_x

	__asm
	{
		popad
		jmp HKRET_RadarPosition_X_Border2
	}
}

void __declspec (naked) HOOK_RadarPosition_X_Border3(void) //0058A8E7
{
	_asm pushad
	if (DrawRadarBorder())
		_asm fld _radar_x

	__asm
	{
		popad
		jmp HKRET_RadarPosition_X_Border3
	}
}

void __declspec (naked) HOOK_RadarPosition_X_Border4(void) //0058A988
{
	_asm pushad
	if (DrawRadarBorder())
		_asm fld _radar_x

	__asm
	{
		popad
		jmp HKRET_RadarPosition_X_Border4
	}
}

void __declspec (naked) HOOK_HightBarPosition_X(void) //0058A5E0
{
	_asm pushad
	radar->CalcRadarPosition();

	__asm
	{
		popad
		fld _radar_x
		jmp HKRET_HightBarPosition_X
	}
}

void __declspec (naked) HOOK_HightBarPosition_X_separator(void) //0058A6E4
{
	_asm pushad
	radar->CalcRadarPosition();

	__asm
	{
		popad
		fld _radar_x
		jmp HKRET_HightBarPosition_X_separator
	}
}

void __declspec (naked) HOOK_FlyHudPosition_X(void) //0058A467
{
	_asm pushad
	radar->CalcRadarPosition();

	__asm
	{
		popad
		fmul _radar_x
		jmp HKRET_FlyHudPosition_X
	}
}

float GetRadarX(float &SX, float &SY, float &RW)
{
	if (SX / SY > 1.3 && SX/SY < 1.35)
	{
		float dummy, ret;

		SF->getGame()->convertWindowCoordsToGame(SX, 0, &ret, &dummy);
		return ret - (RW - 40.0);
	}

	return (SX / (SY / _RadarOffset)) - (RW + 40.0);
}

bool DrawRadarBorder()
{
	if (radar->GetBorder())
		return true;
	return false;
}

void Radar::CalcRadarPosition()
{
	float ScreenX = *(int*)0x00C9C040;
	float ScreenY = *(int*)0x00C9C044;
	float RadarW = *(float*)0x00866B78;
	//float RadarH = *(float*)0x00866B74;

	switch (mode)
	{
	case 1:
		_radar_x = GetRadarX(ScreenX, ScreenY, RadarW);
		*(float*)0x00866B70 = 104.0f;
		break;
	case 2:
		_radar_x = GetRadarX(ScreenX, ScreenY, RadarW);
		*(float*)0x00866B70 = 420.0f;
		break;
	case 3:
		_radar_x = 40.0f;
		*(float*)0x00866B70 = 420.0f;
		break;

	default:
		_radar_x = 40.0f;
		*(float*)0x00866B70 = 104.0f;
		break;
	}
}

void Radar::SetMode(byte mode)
{
	this->mode = mode;
}

byte Radar::GetMode()
{
	return mode;
}

void Radar::SetBorder(bool &Border)
{
	this->border = Border;
}

bool Radar::GetBorder()
{
	return border;
}

Radar::Radar()
{
	//radar
	memcpy_safe(BACKUP_RadarPosition_X, (void*)0x005834D2, 6);

	SF->getGame()->createHook((void *)0x005834D2, HOOK_RadarPosition_X, DETOUR_TYPE_JMP, 6);


	//borders
	memcpy_safe(BACKUP_RadarPosition_X_Border1, (void*)0x0058A799, 6);
	memcpy_safe(BACKUP_RadarPosition_X_Border2, (void*)0x0058A834, 6);
	memcpy_safe(BACKUP_RadarPosition_X_Border3, (void*)0x0058A8E7, 6);
	memcpy_safe(BACKUP_RadarPosition_X_Border4, (void*)0x0058A988, 6);

	SF->getGame()->createHook((void *)0x0058A799, HOOK_RadarPosition_X_Border1, DETOUR_TYPE_JMP, 6);
	SF->getGame()->createHook((void *)0x0058A834, HOOK_RadarPosition_X_Border2, DETOUR_TYPE_JMP, 6);
	SF->getGame()->createHook((void *)0x0058A8E7, HOOK_RadarPosition_X_Border3, DETOUR_TYPE_JMP, 6);
	SF->getGame()->createHook((void *)0x0058A988, HOOK_RadarPosition_X_Border4, DETOUR_TYPE_JMP, 6);


	//hight bar
	memcpy_safe(BACKUP_HightBarPosition_X, (void*)0x0058A5E0, 6);
	memcpy_safe(BACKUP_HightBarPosition_X_separator, (void*)0x0058A6E4, 6);

	SF->getGame()->createHook((void *)0x0058A5E0, HOOK_HightBarPosition_X, DETOUR_TYPE_JMP, 6);
	SF->getGame()->createHook((void *)0x0058A6E4, HOOK_HightBarPosition_X_separator, DETOUR_TYPE_JMP, 6);


	//fly hud
	memcpy_safe(BACKUP_FlyHudPosition_X, (void*)0x0058A467, 6);

	SF->getGame()->createHook((void *)0x0058A467, HOOK_FlyHudPosition_X, DETOUR_TYPE_JMP, 6);


	mode = 0;
	border = true;
}

Radar::~Radar()
{
	//radar
	memcpy_safe((void*)0x005834D2, BACKUP_RadarPosition_X, 6);
	*(float*)0x00866B70 = 104.0f;

	//borders
	memcpy_safe((void*)0x0058A799, BACKUP_RadarPosition_X_Border1, 6);
	memcpy_safe((void*)0x0058A834, BACKUP_RadarPosition_X_Border2, 6);
	memcpy_safe((void*)0x0058A8E7, BACKUP_RadarPosition_X_Border3, 6);
	memcpy_safe((void*)0x0058A988, BACKUP_RadarPosition_X_Border4, 6);

	//hight bar
	memcpy_safe((void*)0x0058A5E0, BACKUP_HightBarPosition_X, 6);
	memcpy_safe((void*)0x0058A6E4, BACKUP_HightBarPosition_X_separator, 6);

	//fly hud
	memcpy_safe((void*)0x0058A467, BACKUP_FlyHudPosition_X, 6);
}
