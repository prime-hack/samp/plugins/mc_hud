#include "main.h"
std::string convToLowercase(char *str)
{
	int sz = strlen(str);
	char ch[1024];
	memset(ch, 0, sizeof(ch));

	strcpy(ch, str);
	for (int i = 0; i < sz; i++)
	{
		if (ch[i] >= 0x41 && ch[i] <= 0x5a) ch[i] += 0x20;
		else if (ch[i] >= 0xc0 && ch[i] <= 0xdf) ch[i] += 0x20;
	}

	return (const char*)ch;
}