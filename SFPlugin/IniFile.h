bool file_exist(char* file_name);

class IniFile //made by MISTER_GONWIK
{
public:
	IniFile(std::string ini_file_name, std::string ini_file_path, bool bcreate = true)
	{
		//if (ini_file_name.find(".ini") == std::string::npos)
		//	m_sIniFileName = ini_file_name + ".ini";
		//else
			m_sIniFileName = ini_file_name;
		if (ini_file_path.empty())
			m_sIniFilePath = getWorkingDirectory();
		else
		{
			createDirectory(ini_file_path);
			m_sIniFilePath = ini_file_path;
		}
		if (bcreate)
			create_new_ini_file();
	}
	~IniFile() {};

	bool	deleteIniFile()
	{
		if (ini_file_exist())
		{
			remove(getIniFileFullPath().c_str());
			return true;
		}
		return false;
	}
	std::string	getIniFileName()
	{
		return m_sIniFileName;
	}
	std::string	getIniFilePath()
	{
		return m_sIniFilePath;
	}
	std::string	getIniFileFullPath()
	{
		return m_sIniFilePath + m_sIniFileName;
	}

	std::string getWorkingDirectory()
	{
		char ppath[MAX_PATH];
		GetCurrentDirectory(MAX_PATH, ppath);
		return std::string(ppath) + "\\";
	};
	std::string createDirectory(std::string ini_file_directory)
	{
		size_t first_splash = ini_file_directory.find_first_of('\\'), next_splash = std::string::npos;
		std::string dir = ini_file_directory.substr(0, first_splash);
		ini_file_directory = ini_file_directory.substr(first_splash, ini_file_directory.length());
		while ((next_splash = ini_file_directory.find("\\", 1)) != std::string::npos)
		{
			dir = dir + ini_file_directory.substr(0, next_splash);
			ini_file_directory = ini_file_directory.substr(next_splash, ini_file_directory.length());
			CreateDirectory(dir.c_str(), NULL);
		}
		return dir + "\\";
	}

	//SECTIONS
	bool existSection(std::string section_name)
	{
		DWORD asd = GetPrivateProfileString(section_name.c_str(), NULL, "", szBuffer, 3000, getIniFileFullPath().c_str());
		return (asd > 0) ? true : false;
	}
	std::vector <std::string> readSections()
	{
		int sections_count = ::GetPrivateProfileSectionNames(szBuffer, 3000, getIniFileFullPath().c_str());
		for (int i = 0; i < sections_count; i++)
			if (szBuffer[i] == '\0') szBuffer[i] = '\\';

		std::string sBuffer = szBuffer;
		std::vector <std::string> sections;
		size_t first_splash = 0, next_splash = std::string::npos;
		while ((next_splash = sBuffer.find_first_of('\\')) != std::string::npos)
		{
			sections.push_back(sBuffer.substr(first_splash, next_splash));
			sBuffer = sBuffer.substr(next_splash + 1, sBuffer.length());
		}
		return sections;
	}
	void deleteSection(std::string section_name)
	{
		WritePrivateProfileString(section_name.c_str(), NULL, NULL, getIniFileFullPath().c_str());
	}

	//KEYS
	bool existKey(std::string section_name, std::string key_name)
	{
		DWORD asd = GetPrivateProfileString(section_name.c_str(), key_name.c_str(), "", szBuffer, 3000, getIniFileFullPath().c_str());
		return (asd > 0) ? true : false;
	}
	std::vector <std::string> readKeys(std::string section_name)
	{
		int keys_count = ::GetPrivateProfileSection(section_name.c_str(), szBuffer, 3000, getIniFileFullPath().c_str());
		for (int i = 0; i < keys_count; i++)
			if (szBuffer[i] == '\0') szBuffer[i] = '\\';

		std::string sBuffer = szBuffer;
		std::vector <std::string> keys;
		size_t first_splash = 0, next_splash = std::string::npos;
		while ((next_splash = sBuffer.find_first_of('\\')) != std::string::npos)
		{
			keys.push_back(sBuffer.substr(first_splash, next_splash));
			sBuffer = sBuffer.substr(next_splash + 1, sBuffer.length());
		}
		for (size_t i = 0; i < keys.size(); i++)
			keys[i] = keys[i].substr(0, keys[i].find_first_of('='));

		return keys;
	}
	void deleteKey(std::string section_name, std::string key_name)
	{
		WritePrivateProfileString(section_name.c_str(), key_name.c_str(), NULL, getIniFileFullPath().c_str());
	}

	//GET FUNCTIONS
	int IniFile::getInteger(std::string section_name, std::string key_name)
	{
		if (!ini_file_exist() || !existSection(section_name) || !existKey(section_name, key_name))
			return 0x80000000;

		return GetPrivateProfileInt(section_name.c_str(), key_name.c_str(), 0, getIniFileFullPath().c_str());
	}
	float IniFile::getFloat(std::string section_name, std::string key_name)
	{
		if (!ini_file_exist() || !existSection(section_name) || !existKey(section_name, key_name))
			return 0.0f;

		GetPrivateProfileString(section_name.c_str(), key_name.c_str(), NULL, szBuffer, sizeof(szBuffer), getIniFileFullPath().c_str());
		return std::stof(szBuffer);
	}
	std::string IniFile::getString(std::string section_name, std::string key_name)
	{
		if (!ini_file_exist() || !existSection(section_name) || !existKey(section_name, key_name))
			return "";

		GetPrivateProfileString(section_name.c_str(), key_name.c_str(), NULL, szBuffer, sizeof(szBuffer), getIniFileFullPath().c_str());
		return szBuffer;
	}
	bool IniFile::getBoolean(std::string section_name, std::string key_name)
	{
		if (!ini_file_exist() || !existSection(section_name) || !existKey(section_name, key_name))
			return false;

		GetPrivateProfileString(section_name.c_str(), key_name.c_str(), NULL, szBuffer, sizeof(szBuffer), getIniFileFullPath().c_str());
		if (!_stricmp("true", (char*)convToLowercase(szBuffer).c_str()) ||
			!_stricmp("1", (char*)convToLowercase(szBuffer).c_str()))
			return true;
		return false;
	}
	DWORD IniFile::getHex(std::string section_name, std::string key_name)
	{
		if (!ini_file_exist() || !existSection(section_name) || !existKey(section_name, key_name))
			return 0;

		GetPrivateProfileString(section_name.c_str(), key_name.c_str(), NULL, szBuffer, sizeof(szBuffer), getIniFileFullPath().c_str());
		DWORD HEX = 0;
		sscanf(szBuffer, "%X", &HEX);
		return HEX;
	}
	//ARRAYS
	std::vector <std::string> getArrayString(std::string section_name, std::string key_name)
	{
		int keys_count = ::GetPrivateProfileSection(section_name.c_str(), szBuffer, 3000, getIniFileFullPath().c_str());
		for (int i = 0; i < keys_count; i++)
			if (szBuffer[i] == '\0') szBuffer[i] = '\\';

		std::string sBuffer = szBuffer, szBuff;
		std::vector <std::string> keys_value;
		size_t first_splash = 0, next_splash = std::string::npos, unk = std::string::npos;
		while ((next_splash = sBuffer.find_first_of('\\')) != std::string::npos)
		{
			unk = sBuffer.find_first_of('=');
			szBuff = sBuffer.substr(first_splash, unk);
			if (!strcmp(szBuff.c_str(), key_name.c_str()))
				keys_value.push_back(sBuffer.substr(unk + 1, next_splash - unk - 1));
			sBuffer = sBuffer.substr(next_splash + 1, sBuffer.length());
		}

		return keys_value;
	}
	void setArrayString(std::string key_name, std::vector <std::string> value)
	{
		FILE *f;
		f = fopen(getIniFileFullPath().c_str(), "at");
		char str[1000], tmp[1000];
		for (int i = 0; i < value.size(); i++)
		{
			memset(str, 0, sizeof(str));
			if (!i)sprintf(str, "%s = \"%s\"", key_name.c_str(), value[i].c_str());
			else sprintf(str, "%s \"%s\"", tmp, value[i].c_str());
			memset(tmp, 0, sizeof(tmp));
			strcpy(tmp, str);
		}
		strcat(str, "\n");
		size_t size = 0;
		while (str[size] != 0)
			size++;
		fwrite(str, size, 1, f);
		fclose(f);
	}
	std::vector <int> getArrayInt(std::string section_name, std::string key_name)
	{
		int keys_count = ::GetPrivateProfileSection(section_name.c_str(), szBuffer, 3000, getIniFileFullPath().c_str());
		for (int i = 0; i < keys_count; i++)
			if (szBuffer[i] == '\0') szBuffer[i] = '\\';

		std::string sBuffer = szBuffer, szBuff;
		std::vector <int> keys_int_value;
		size_t first_splash = 0, next_splash = std::string::npos, unk = std::string::npos;
		while ((next_splash = sBuffer.find_first_of('\\')) != std::string::npos)
		{
			unk = sBuffer.find_first_of('=');
			szBuff = sBuffer.substr(first_splash, unk);
			if (!strcmp(szBuff.c_str(), key_name.c_str()))
				keys_int_value.push_back(atoi(sBuffer.substr(unk + 1, next_splash - unk - 1).c_str()));
			sBuffer = sBuffer.substr(next_splash + 1, sBuffer.length());
		}

		return keys_int_value;
	}
	std::vector <float> getArrayFloat(std::string section_name, std::string key_name)
	{
		int keys_count = ::GetPrivateProfileSection(section_name.c_str(), szBuffer, 3000, getIniFileFullPath().c_str());
		for (int i = 0; i < keys_count; i++)
			if (szBuffer[i] == '\0') szBuffer[i] = '\\';

		std::string sBuffer = szBuffer, szBuff;
		std::vector <float> keys_float_value;
		size_t first_splash = 0, next_splash = std::string::npos, unk = std::string::npos;
		while ((next_splash = sBuffer.find_first_of('\\')) != std::string::npos)
		{
			unk = sBuffer.find_first_of('=');
			szBuff = sBuffer.substr(first_splash, unk);
			if (!strcmp(szBuff.c_str(), key_name.c_str()))
				keys_float_value.push_back(std::stof(sBuffer.substr(unk + 1, next_splash - unk - 1).c_str()));
			sBuffer = sBuffer.substr(next_splash + 1, sBuffer.length());
		}

		return keys_float_value;
	}

	std::vector <std::string> getParams(std::string str)
	{
		std::vector <std::string> params;
		size_t start_ip = std::string::npos, end_ip = std::string::npos;
		while ((start_ip = str.find('"')) != std::string::npos)
		{
			end_ip = str.find('"', start_ip + 1);
			params.push_back(str.substr(start_ip + 1, end_ip - 1));
			str = str.erase(0, str.find('"', end_ip + 1));
		}
		return params;
	}
	std::vector <std::string> getParams(std::string str, char *delimiter)
	{
		std::vector <std::string> params;
		size_t start_ip = std::string::npos, end_ip = std::string::npos;
		while ((start_ip = str.find(delimiter)) != std::string::npos)
		{
			end_ip = str.find(delimiter, start_ip + 1);
			params.push_back(str.substr(start_ip + 1, end_ip - 1));
			str = str.erase(0, str.find(delimiter, end_ip + 1));
		}
		return params;
	}
	std::vector <std::string> getParams(std::string str, char *s_delimiter, char *e_delimiter)
	{
		size_t slen = strlen(s_delimiter), elen = strlen(s_delimiter);
		std::vector <std::string> params;
		size_t start_ip = std::string::npos, end_ip = std::string::npos;
		while ((start_ip = str.find(s_delimiter)) != std::string::npos)
		{
			end_ip = str.find(e_delimiter, start_ip + slen);
			params.push_back(str.substr(start_ip + slen, end_ip - elen));
			str = str.erase(0, str.find(s_delimiter, end_ip + slen));
		}
		return params;
	}

	//SET FUNCTIONS
	void IniFile::setInteger(std::string section_name, std::string key_name, DWORD value)
	{
		if (!ini_file_exist()) return;

		WritePrivateProfileString(section_name.c_str(), key_name.c_str(), std::to_string(value).c_str(), getIniFileFullPath().c_str());
	}
	void IniFile::setFloat(std::string section_name, std::string key_name, float value)
	{
		if (!ini_file_exist()) return;

		WritePrivateProfileString(section_name.c_str(), key_name.c_str(), std::to_string(value).c_str(), getIniFileFullPath().c_str());
	}
	void IniFile::setString(std::string section_name, std::string key_name, std::string text)
	{
		if (!ini_file_exist()) return;

		WritePrivateProfileString(section_name.c_str(), key_name.c_str(), text.c_str(), getIniFileFullPath().c_str());
	}
	void IniFile::setBoolean(std::string section_name, std::string key_name, bool value)
	{
		if (!ini_file_exist()) return;

		if (value) WritePrivateProfileString(section_name.c_str(), key_name.c_str(), value ? "true" : "false", getIniFileFullPath().c_str());
	}
	void IniFile::setHex(std::string section_name, std::string key_name, DWORD HEX)
	{
		if (!ini_file_exist()) return;
		char szBuf[32];
		sprintf(szBuf, "%X", HEX);

		WritePrivateProfileString(section_name.c_str(), key_name.c_str(), szBuf, getIniFileFullPath().c_str());
	}

	//ADD FUNCTIONS
	void IniFile::addInteger(std::string section_name, std::string key_name, DWORD value)
	{
		if (!ini_file_exist()) return;
		if (!existKey(section_name, key_name))
			WritePrivateProfileString(section_name.c_str(), key_name.c_str(), std::to_string(value).c_str(), getIniFileFullPath().c_str());
	}
	void IniFile::addFloat(std::string section_name, std::string key_name, float value)
	{
		if (!ini_file_exist()) return;
		if (!existKey(section_name, key_name))
			WritePrivateProfileString(section_name.c_str(), key_name.c_str(), std::to_string(value).c_str(), getIniFileFullPath().c_str());
	}
	void IniFile::addString(std::string section_name, std::string key_name, std::string text)
	{
		if (!ini_file_exist()) return;
		if (!existKey(section_name, key_name))
			WritePrivateProfileString(section_name.c_str(), key_name.c_str(), text.c_str(), getIniFileFullPath().c_str());
	}
	void IniFile::addBoolean(std::string section_name, std::string key_name, bool value)
	{
		if (!ini_file_exist()) return;
		if (!existKey(section_name, key_name))
			if (value) WritePrivateProfileString(section_name.c_str(), key_name.c_str(), value ? "true" : "false", getIniFileFullPath().c_str());
	}
	void IniFile::addHex(std::string section_name, std::string key_name, DWORD HEX)
	{
		if (!ini_file_exist()) return;
		char szBuf[32];
		sprintf(szBuf, "%X", HEX);

		if (!existKey(section_name, key_name))
			WritePrivateProfileString(section_name.c_str(), key_name.c_str(), szBuf, getIniFileFullPath().c_str());
	}

	int IniFile::esgInteger(std::string section_name, std::string key_name, DWORD value)
	{
		if (!ini_file_exist()) return NULL;
		if (!existKey(section_name, key_name))
		{
			WritePrivateProfileString(section_name.c_str(), key_name.c_str(), std::to_string(value).c_str(), getIniFileFullPath().c_str());
			return value;
		}
		return GetPrivateProfileInt(section_name.c_str(), key_name.c_str(), 0, getIniFileFullPath().c_str());
	}
	float IniFile::esgFloat(std::string section_name, std::string key_name, float value)
	{
		if (!ini_file_exist()) return NULL;
		if (!existKey(section_name, key_name))
		{
			WritePrivateProfileString(section_name.c_str(), key_name.c_str(), std::to_string(value).c_str(), getIniFileFullPath().c_str());
			return value;
		}
		GetPrivateProfileString(section_name.c_str(), key_name.c_str(), NULL, szBuffer, sizeof(szBuffer), getIniFileFullPath().c_str());
		return std::stof(szBuffer);
	}
	std::string IniFile::esgString(std::string section_name, std::string key_name, std::string text)
	{
		if (!ini_file_exist()) return NULL;
		if (!existKey(section_name, key_name))
		{
			WritePrivateProfileString(section_name.c_str(), key_name.c_str(), text.c_str(), getIniFileFullPath().c_str());
			return text;
		}
		GetPrivateProfileString(section_name.c_str(), key_name.c_str(), NULL, szBuffer, sizeof(szBuffer), getIniFileFullPath().c_str());
		return szBuffer;
	}
	bool IniFile::esgBoolean(std::string section_name, std::string key_name, bool value)
	{
		if (!ini_file_exist()) return false;
		if (!existKey(section_name, key_name))
		{
			WritePrivateProfileString(section_name.c_str(), key_name.c_str(), value ? "true" : "false", getIniFileFullPath().c_str());
			return value;
		}
		GetPrivateProfileString(section_name.c_str(), key_name.c_str(), NULL, szBuffer, sizeof(szBuffer), getIniFileFullPath().c_str());
		std::string str = (const char*)szBuffer;
		if (!_stricmp("true", (char*)convToLowercase(szBuffer).c_str()) ||
			!_stricmp("1", (char*)convToLowercase(szBuffer).c_str()))
			return true;
		return false;
	}
	DWORD IniFile::esgHex(std::string section_name, std::string key_name, DWORD HEX)
	{
		if (!ini_file_exist()) return 0;
		char szBuf[32];
		sprintf(szBuf, "%X", HEX);

		if (!existKey(section_name, key_name))
		{
			WritePrivateProfileString(section_name.c_str(), key_name.c_str(), szBuf, getIniFileFullPath().c_str());
			return HEX;
		}
		GetPrivateProfileString(section_name.c_str(), key_name.c_str(), NULL, szBuffer, sizeof(szBuffer), getIniFileFullPath().c_str());
		sscanf(szBuffer, "%X", &HEX);
		return HEX;
	}

private:
	char szBuffer[3000];
	std::string m_sIniFilePath;
	std::string m_sIniFileName;

	bool ini_file_exist()
	{
		WIN32_FIND_DATA FindFileData;
		HANDLE hFindIni;

		hFindIni = FindFirstFile(getIniFileFullPath().c_str(), &FindFileData);
		if (hFindIni != INVALID_HANDLE_VALUE)
			return true;

		return false;
	}
	bool create_new_ini_file()
	{
		if (!ini_file_exist())
		{
			FILE *fp = fopen(getIniFileFullPath().c_str(), "w");
			if (fp != NULL)
				fclose(fp);

			return true;
		}
		return false;
	}
};
extern IniFile *ini;