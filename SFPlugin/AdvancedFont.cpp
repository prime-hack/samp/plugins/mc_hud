#include "main.h"


CAdvancedFont::CAdvancedFont(IDirect3DDevice9 *pDevice, stFontInfo *pFont) : CCreateTexture(pDevice, 2048, pFont->DrawHeight())
{
	this->pDevice = pDevice;
	this->pFont = pFont;
}


CAdvancedFont::~CAdvancedFont()
{

}


void CAdvancedFont::Printf(int X, int Y, int W, int H, D3DCOLOR color, const char* szText, float R , bool shadow)
{
	if (isReleased){
		Init();
		return;
	}

	if (isRenderToTexture)
		return;

	Begin();
	Clear();
	SF->getRender()->getD3DDevice()->GetSamplerState(NULL, D3DSAMP_MINFILTER, &LinarState);
	SF->getRender()->getD3DDevice()->SetSamplerState(NULL, D3DSAMP_MINFILTER, D3DTEXF_LINEAR);
	if (!shadow)
		pFont->Print(szText, color, 0, 0);
	else pFont->PrintShadow(szText, color, 0, 0);
	SF->getRender()->getD3DDevice()->SetSamplerState(NULL, D3DSAMP_MINFILTER, LinarState);
	End();

	D3DXMATRIX			mat;

	D3DXVECTOR2 axisPos = D3DXVECTOR2(X, Y);
	D3DXVECTOR2 size(1 / pFont->DrawLength(szText) * W, 1 / (float)surfaceDesc.Height * H);
	D3DXVECTOR2 axisCenter = D3DXVECTOR2(0, pFont->DrawHeight());
	D3DXMatrixTransformation2D(&mat, &axisCenter, 0.0f, &size, &axisCenter, R, &axisPos);

	pSprite->Begin(D3DXSPRITE_ALPHABLEND | D3DXSPRITE_SORT_TEXTURE | D3DXSPRITE_DO_NOT_ADDREF_TEXTURE | D3DXSPRITE_SORT_DEPTH_FRONTTOBACK);
	pSprite->SetTransform(&mat);
	pSprite->Draw(pTexture, NULL, NULL, NULL, -1);
	pSprite->End();
}


void CAdvancedFont::Printf(int X, int Y, int W, int H, D3DCOLOR color, const char* szText, bool shadow, float R)
{
	Printf(X, Y, W, H, color, szText, shadow, R);
}


void CAdvancedFont::Print(int X, int Y, D3DCOLOR color, float R, bool shadow, const char* szText, ...)
{
	va_list ap;
	char	*szStr = new char[strlen(szText) * 2 + 0x100];
	va_start(ap, szText);
	vsprintf(szStr, szText, ap);
	va_end(ap);

	Printf(X, Y, (int)pFont->DrawLength(szText), (int)pFont->DrawHeight(), color, szStr, R, shadow);
	delete[] szStr;
}


void CAdvancedFont::Print(int X, int Y, D3DCOLOR color, const char* szText, float R, bool shadow)
{
	Print(X, Y, color, R, shadow, szText);
}


void CAdvancedFont::Print(int X, int Y, D3DCOLOR color, const char* szText, bool shadow, float R)
{
	Print(X, Y, color, R, shadow, szText);
}


/*
void CAdvancedFont::ReInit(stFontInfo *pFont)
{
	this->pFont = pFont;
	//CCreateTexture::ReInit(2048, pFont->DrawHeight());

	Release();
	textureSize = { 2048, pFont->DrawHeight() };
	Init();
}*/


bool MouseInBox(POINT MP, int X, int Y, int W, int H, float R = 0.0f)
{
	if (R == 0.0 && MP.x > X && MP.x < X + W && MP.y > Y && MP.y < Y + H)
		return true;

	POINT A = { X, Y };
	POINT B = { cos(R)*(W + X) + sin(R)*(H + Y), cos(R)*(H + Y) - sin(R)*(W + X) };
	POINT C = { sin(R)*(H + Y), cos(R)*(H + Y) };
	POINT D = { cos(R)*(W + X), cos(R)*(H + Y) - sin(R)*(W + X) - cos(R)*(H + Y) };

	int a = (A.x - MP.x)*(B.y - A.y) - (B.x - A.x)*(A.y - MP.y);
	int bc = (B.x - MP.x)*(C.y - B.y) - (C.x - B.x)*(B.y - MP.y);
	int bd = (B.x - MP.x)*(D.y - B.y) - (D.x - B.x)*(B.y - MP.y);
	int c = (C.x - MP.x)*(A.y - C.y) - (A.x - C.x)*(C.y - MP.y);
	int d = (D.x - MP.x)*(A.y - D.y) - (A.x - D.x)*(D.y - MP.y);

	if ( ( a > 0 && ( (bc > 0 && c > 0) || (bd > 0 && d > 0) ) ) ||
		 ( a < 0 && ( (bc < 0 && c < 0) || (bd < 0 && d < 0) ) ) )
		return true;
	return false;
}