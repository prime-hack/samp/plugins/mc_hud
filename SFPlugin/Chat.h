#define SAMP_PATCH_CHATINPUTADJUST_Y				0x63FE6
#define SAMP_PATCH_CHATPOS_Y						0x63da0
class Chat
{
	DWORD dwSAMP_Addr;
	DWORD dwChatInputBox_Y;
	byte BKRET_ChatPos[4];
	byte BKRET_CHATINPUTADJUST_Y[6];
	bool bClassic;
	bool bMiniChat;
public:
	Chat();
	void SetMode(bool classic);
	bool GetMode();
	void SetMini(bool &MiniChat);
	bool GetMini();
	void RenderChat();
	~Chat();
};

extern Chat *chat;
