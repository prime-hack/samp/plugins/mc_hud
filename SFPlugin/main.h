#include <windows.h>
#include <string>
#include <assert.h>
#include <process.h>

#include "SAMPFUNCS_API.h"
#include "game_api\game_api.h"

extern SAMPFUNCS *SF;
extern stFontInfo *pFont;

#include "Radar.h"
#include "Proc.h"
#include "Chat.h"
#include "Keys.h"
#include "IniFile.h"
#include "HUD.h"
#include "!MenuManager.h"
#include "CreateTexture.h"
#include "AdvancedFont.h"

void CALLBACK Destructor();

struct stMenuTranslate
{
	bool MC_HUD;
	bool ClassicDraw;
	bool MiniChat;
	bool Border;
	byte MAP;
};