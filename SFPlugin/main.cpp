#include "main.h"
SAMPFUNCS *SF = new SAMPFUNCS();
stFontInfo *pFont;
Radar *radar;
Chat *chat;
IniFile *ini = new IniFile("SAMPFUNCS\\MC_HUD 2.5\\Settings.ini", "");
keyboard keyb;
HUD *hud;
Menu *menu; //���� ���������� ����� ����, ��� ���������� ��� ����������� �� �������������
stMenuTranslate translate;
CCreateTexture *tex;
CAdvancedFont *CFont;

bool CALLBACK Present(CONST RECT *pSourceRect, CONST RECT *pDestRect, HWND hDestWindowOverride, CONST RGNDATA *pDirtyRegion)
{
	if (SUCCEEDED(SF->getRender()->BeginRender())) // ���� ������ ����� � ���������
	{
		if (!SF->getGame()->isGTAMenuActive())
		{

			//������
			//tex->Init();
			tex->Begin();
			tex->Clear();

			//���������� ������
			DWORD LinarState;
			SF->getRender()->getD3DDevice()->GetSamplerState(NULL, D3DSAMP_MINFILTER, &LinarState);
			SF->getRender()->getD3DDevice()->SetSamplerState(NULL, D3DSAMP_MINFILTER, D3DTEXF_LINEAR);
			chat->RenderChat();
			SF->getRender()->getD3DDevice()->SetSamplerState(NULL, D3DSAMP_MINFILTER, LinarState);

			hud->Render();

			tex->End();
			tex->Render(0, 0);
			//tex->Release();

			CFont->Print(50, 50, *(int*)0xC9C040 - 100, (int)pFont->DrawHeight(), -1, "Hello text", 0.0f);
			CFont->Print(50, 50, -1, "Hello text", 1.57f);
		}
		else if (SF->getSAMP()->getMisc()->iCursorMode)
			SF->getSAMP()->getMisc()->ToggleCursor(false);

		SF->getRender()->EndRender(); // ��������� ���������
	}
	return true;
};

bool CALLBACK Reset( D3DPRESENT_PARAMETERS *pPresentationParameters )
{
	tex->Release();
	CFont->Release();
	/*if (tex != nullptr){
		delete tex;
		tex = nullptr;
	}*/
	return true;
}

WNDPROC hOrigProc;
LRESULT APIENTRY WndProcHook(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
	/*if (uMsg == WM_SIZE)
		tex->ReInit(*(int*)0xC9C040, *(int*)0xC9C044);*/

	return CallWindowProc(hOrigProc, hwnd, uMsg, wParam, lParam);
}

std::string RadarPos()
{
	switch (radar->GetMode())
	{
	case 0:
		return "{FF40FF40}left-lower";
	case 1:
		return "{FF40FF40}right-lower";
	case 2:
		return "{FF40FF40}right-upper";
	case 3:
		return "{FF40FF40}left-upper";
	default:
		return "{FFFF4040}error";
	}
}

void CALLBACK SwitchRadarPos(int key)
{
	if (radar->GetMode() != 3)
		radar->SetMode(radar->GetMode() + 1);
	else radar->SetMode(0);

	menu->Elements->SetElementText(3, RadarPos().c_str());
}

bool Destructing = false;
void CALLBACK mainloop()
{
	if (Destructing)
		return;
	static bool init = false;
	static DWORD MenuKey;
	if (!init)
	{
		if (GAME == nullptr)
			return;
		if (!SF->getSAMP()->IsInitialized())
			return;
		if (!MenuLoaded())
			return;

		ScreenFont();
		tex = new CCreateTexture(SF->getRender()->getD3DDevice(), *(int*)0xC9C040, *(int*)0xC9C044);
		CFont = new CAdvancedFont(SF->getRender()->getD3DDevice(), pFont);
		SF->getRender()->registerD3DCallback(eDirect3DDeviceMethods::D3DMETHOD_PRESENT, Present);
		SF->getRender()->registerD3DCallback(eDirect3DDeviceMethods::D3DMETHOD_RESET, Reset);
		hOrigProc = (WNDPROC)SetWindowLong(GetActiveWindow(), GWL_WNDPROC, (LONG)(UINT_PTR)WndProcHook);

		radar = new Radar();
		chat = new Chat();
		hud = new HUD();
		menu = new Menu("MC_HUD", 100, 100, 165, 135, 0xC0FFFFFF, -1, false, true);

		translate.ClassicDraw = ini->esgBoolean("CHAT", "ClassicDraw", false);
		translate.MiniChat = ini->esgBoolean("CHAT", "MC_Mini", true);

		translate.MAP = ini->esgInteger("RADAR", "POS", 3);
		if (translate.MAP > 3) translate.MAP = 3;
		translate.Border = ini->esgBoolean("RADAR", "Border", true);

		translate.MC_HUD = ini->esgBoolean("MC_HUD", "Enable", true);
		MenuKey = keyb.GetKeyNum(ini->esgString("MC_HUD", "MenuKey", "Home"));

		chat->SetMode(translate.ClassicDraw);
		chat->SetMini(translate.MiniChat);
		radar->SetMode(translate.MAP);
		radar->SetBorder(translate.Border);
		hud->SetHudState(translate.MC_HUD);

		menu->Tumblers->AddTumbler("Minecraft HUD", 5, 5, &translate.MC_HUD);
		menu->Elements->AddElement("{FFFFFF60}CHAT:", 5, 20, NULL);
		menu->Tumblers->AddTumbler("Classic draw", 5, 35, &translate.ClassicDraw);
		menu->Tumblers->AddTumbler("Minichat for MC mode", 5, 50, &translate.MiniChat);
		menu->Elements->AddElement("{FFFFFF60}RADAR:", 5, 65, NULL);
		menu->Tumblers->AddTumbler("Border on map", 5, 80, &translate.Border);
		menu->Elements->AddElement("POS:", 5, 95, NULL);
		menu->Elements->AddElement((char*)RadarPos().c_str(), 45, 95, SwitchRadarPos);

		if (file_exist("SAMPFUNCS\\MC_HUD 2.5\\Background.png"))
			menu->SetTextureBackground("SAMPFUNCS\\MC_HUD 2.5\\Background.png");

		//SF->getGame()->registerGameDestructorCallback(Destructor);
		init = true;
	}
	else
	{
		static int wUpdate = 0; //update score for hud
		if (playerIsSpawned() && wUpdate < GetTickCount())
		{
			SF->getSAMP()->getInfo()->UpdateScoreAndPing();
			wUpdate = GetTickCount() + 200;
		}

		hud->ApplyDamage();

		if (SF->isConsoleOpened())
			return;

		if (SF->getSAMP()->getInput()->iInputEnabled)
			return;

		static bool bClosed = true;
		if (SF->getGame()->isKeyPressed(MenuKey))
		{
			menu->SetPresent(menu->IsPresent() ^ true);
			toggleCursor(true);
			bClosed = false;
		}

		if (!menu->IsPresent() && GetCursorState() && !bClosed)
		{
			toggleCursor(false);
			bClosed = true;
		}

		if (menu->IsPresent())
		{
			chat->SetMode(translate.ClassicDraw);
			chat->SetMini(translate.MiniChat);
			radar->SetBorder(translate.Border);
			hud->SetHudState(translate.MC_HUD);
		}
	}
}

BOOL APIENTRY DllMain(HMODULE hModule, DWORD dwReasonForCall, LPVOID lpReserved)
{
	switch (dwReasonForCall)
	{
		case DLL_PROCESS_ATTACH:
			SF->initPlugin(mainloop, hModule);
			break;
		case DLL_THREAD_ATTACH:
		case DLL_THREAD_DETACH:
			break;
		case DLL_PROCESS_DETACH:
			Destructor();
			break;
	}
	return TRUE;
}

void CALLBACK Destructor()
{
	Destructing = true;
	SetWindowLong(GetActiveWindow(), GWL_WNDPROC, (LONG)(UINT_PTR)hOrigProc);
	delete tex;
	delete menu;

	delete chat;
	ini->setBoolean("CHAT", "ClassicDraw", chat->GetMode());
	ini->setBoolean("CHAT", "MC_Mini", chat->GetMini());

	ini->setInteger("RADAR", "POS", radar->GetMode());
	ini->setBoolean("RADAR", "Border", radar->GetBorder());

	ini->setBoolean("MC_HUD", "Enable", hud->GetHudState());

	delete radar;
	delete hud;
}