#include "main.h"

void *memcpy_safe(void *_dest, const void *_src, size_t stLen)
{
	if (_dest == nullptr || _src == nullptr || stLen == 0)
		return nullptr;

	MEMORY_BASIC_INFORMATION	mbi;
	VirtualQuery(_dest, &mbi, sizeof(mbi));
	VirtualProtect(mbi.BaseAddress, mbi.RegionSize, PAGE_EXECUTE_READWRITE, &mbi.Protect);

	void	*pvRetn = memcpy(_dest, _src, stLen);
	VirtualProtect(mbi.BaseAddress, mbi.RegionSize, mbi.Protect, &mbi.Protect);
	FlushInstructionCache(GetCurrentProcess(), _dest, stLen);
	return pvRetn;
}

int memset_safe(void *_dest, int c, uint32_t len)
{
	uint8_t *dest = (uint8_t *)_dest;
	uint8_t buf[4096];

	memset(buf, c, (len > 4096) ? 4096 : len);

	for (;;)
	{
		if (len > 4096)
		{
			if (!memcpy_safe(dest, buf, 4096))
				return 0;
			dest += 4096;
			len -= 4096;
		}
		else
		{
			if (!memcpy_safe(dest, buf, len))
				return 0;
			break;
		}
	}

	return 1;
}

void ScreenFont()
{
	int fSize = 8;
	static bool FontDef = false;
	if (FontDef) SF->getRender()->ReleaseFont(pFont);
	else FontDef = true;
	int res = *(int*)0xC9C040;
	if (res <= 640) fSize = 6;
	if (res <= 1200) fSize = 7;
	if (res >= 1300 && res < 1800) fSize = 9;
	if (res >= 1800) fSize = 10;
	pFont = SF->getRender()->CreateNewFont("Tahoma", fSize, 13);
}

float getSkill(int weaponId)
{
	float Skill = -10.0f;
	if ((weaponId >= 22) && (weaponId <= 32))
	{
		if (weaponId == 32)
			weaponId -= 4;
		Skill = *(float*)((weaponId - 22) * 4 + 0xB79494);
	}
	return Skill / 10.0f;
}

bool playerIsSpawned()
{
	if (SF->getSAMP()->getPlayers()->IsPlayerDefined(SF->getSAMP()->getPlayers()->sLocalPlayerID, true))
		return (SF->getSAMP()->getPlayers()->pLocalPlayer->iIsActorAlive && !SF->getSAMP()->getPlayers()->pLocalPlayer->iIsInSpawnScreen);

	return false;
}