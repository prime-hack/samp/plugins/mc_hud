#pragma once
#include <d3dx9core.h>
class CCreateTexture
{
public:
	CCreateTexture(IDirect3DDevice9 *pDevice, const int width, const int height);
	virtual ~CCreateTexture();

	void Begin();
	void End();

	bool Clear(D3DCOLOR color = 0);
	bool Render(const int X, const int Y, int W = 0, int H = 0, const float R = 0.0f);
	HRESULT Save(const char* name);

	void Release();
	void Init();
	/*virtual*/ void ReInit(const int width, const int height);

	ID3DXSprite* GetSprite();
	IDirect3DTexture9* GetTexture();
	D3DSURFACE_DESC GetSurfaceDesc();

protected:
	ID3DXSprite*			pSprite;
	IDirect3DTexture9*		pTexture;
	POINT					textureSize;
	IDirect3DDevice9*		pDevice;
	D3DSURFACE_DESC			surfaceDesc;

	bool					isRenderToTexture;
	bool					isReleased;

private:
	LPDIRECT3DSURFACE9      PP1S = NULL;
	LPDIRECT3DSURFACE9      DS = NULL;
	LPDIRECT3DSURFACE9		OldRT, OldDS;
};

