#pragma comment(lib, "User32")
struct key_alias
{
	int		key;
	char	*name;
};
static struct key_alias key_alias[] =
{
	{ VK_LBUTTON, "lbutton" },
	{ VK_RBUTTON, "rbutton" },
	{ VK_MBUTTON, "mbutton" },
	{ VK_XBUTTON1, "mouse_4" },
	{ VK_XBUTTON2, "mouse_5" },
	{ VK_BACK, "backspace" },
	{ VK_TAB, "tab" },
	{ VK_CAPITAL, "caps" },
	{ VK_RETURN, "return" },
	{ VK_SHIFT, "shift" },
	{ VK_LSHIFT, "lshift" },
	{ VK_RSHIFT, "rshift" },
	{ VK_CONTROL, "ctrl" },
	{ VK_LCONTROL, "lctrl" },
	{ VK_RCONTROL, "rctrl" },
	{ VK_MENU, "alt" },
	{ VK_LMENU, "lalt" },
	{ VK_RMENU, "ralt" },
	{ VK_SPACE, "space" },
	{ VK_PRIOR, "pageup" },
	{ VK_NEXT, "pagedn" },
	{ VK_END, "end" },
	{ VK_HOME, "home" },
	{ VK_LEFT, "left" },
	{ VK_UP, "up" },
	{ VK_RIGHT, "right" },
	{ VK_DOWN, "down" },
	{ VK_INSERT, "insert" },
	{ VK_DELETE, "delete" },
	{ VK_PAUSE, "pause" },
	{ VK_NUMPAD0, "np0" },
	{ VK_NUMPAD1, "np1" },
	{ VK_NUMPAD2, "np2" },
	{ VK_NUMPAD3, "np3" },
	{ VK_NUMPAD4, "np4" },
	{ VK_NUMPAD5, "np5" },
	{ VK_NUMPAD6, "np6" },
	{ VK_NUMPAD7, "np7" },
	{ VK_NUMPAD8, "np8" },
	{ VK_NUMPAD9, "np9" },
	{ VK_MULTIPLY, "np_multiply" },
	{ VK_ADD, "np_plus" },
	{ VK_SEPARATOR, "separator" },
	{ VK_SUBTRACT, "np_minus" },
	{ VK_DECIMAL, "np_decimal" },
	{ VK_DIVIDE, "np_divide" },
	{ VK_F1, "f1" },
	{ VK_F2, "f2" },
	{ VK_F3, "f3" },
	{ VK_F4, "f4" },
	{ VK_F5, "f5" },
	{ VK_F6, "f6" },
	{ VK_F7, "f7" },
	{ VK_F8, "f8" },
	{ VK_F9, "f9" },
	{ VK_F10, "f10" },
	{ VK_F11, "f11" },
	{ VK_F12, "f12" },
	{ VK_F13, "f13" },
	{ VK_F14, "f14" },
	{ VK_F15, "f15" },
	{ VK_F16, "f16" },
	{ VK_F17, "f17" },
	{ VK_F18, "f18" },
	{ VK_F19, "f19" },
	{ VK_F20, "f20" },
	{ VK_F21, "f21" },
	{ VK_F22, "f22" },
	{ VK_F23, "f23" },
	{ VK_F24, "f24" },
	{ VK_OEM_PLUS, "oem_plus" },
	{ VK_OEM_COMMA, "oem_comma" },
	{ VK_OEM_MINUS, "oem_minus" },
	{ VK_OEM_PERIOD, "oem_period" },
	{ VK_OEM_1, "oem_1" },
	{ VK_OEM_2, "oem_2" },
	{ VK_OEM_3, "oem_3" },
	{ VK_OEM_4, "oem_4" },
	{ VK_OEM_5, "oem_5" },
	{ VK_OEM_6, "oem_6" },
	{ VK_OEM_7, "oem_7" },
	{ VK_OEM_8, "oem_8" },
	{ VK_BROWSER_BACK, "bBack" },
	{ VK_BROWSER_FORWARD, "bNext" },
	{ VK_BROWSER_REFRESH, "bRef" },
	{ VK_BROWSER_STOP, "bStop" },
	{ VK_BROWSER_SEARCH, "bSearch" },
	{ VK_BROWSER_FAVORITES, "bFavorit" },
	{ VK_BROWSER_HOME, "bHome" },
	{ VK_VOLUME_MUTE, "Mute" },
	{ VK_VOLUME_UP, "Vol+" },
	{ VK_VOLUME_DOWN, "Vol-" },
	{ VK_MEDIA_NEXT_TRACK, "mNext" },
	{ VK_MEDIA_PREV_TRACK, "mPrev" },
	{ VK_MEDIA_STOP, "mStop" },
	{ VK_MEDIA_PLAY_PAUSE, "mPlayPause" },
	{ VK_LAUNCH_MAIL, "lMail" },
	{ VK_LAUNCH_MEDIA_SELECT, "lMedia" },
	{ VK_LAUNCH_APP1, "lApp1" },
	{ VK_LAUNCH_APP1, "lApp2" },
	{ 0, "&0" },
};
std::string convToLowercase(char *str);
struct keyboard
{
	enum KeyActivate { KeyFalse = 0, KeyTrue, KeyPress };
	KeyActivate KeyActive(byte key)
	{
		if (GetKeyState(key) == 1) return KeyTrue;
		else if (GetKeyState(key) == 0) return KeyFalse;
		return KeyPress;
	}
	int	GetKeyNum(std::string KeyName)
	{
		KeyName = convToLowercase((char*)KeyName.c_str());
		if (KeyName.length() == 1)
		{
			char ch[8];
			sprintf(ch, "%s", KeyName.c_str());
			if (ch[0] >= 0x61 && ch[0] <= 0x7a) return ch[0] - 0x20;
			return ch[0];
		}
		for (int i = 0; key_alias[i].key != NULL; i++)
		{
			size_t pos = strcmp(KeyName.c_str(), convToLowercase(key_alias[i].name).c_str());
			if (pos == 0) return key_alias[i].key;
		}
		return 0;
	}
	std::string	GetKeyName(int KeyNum)
	{
		if (KeyNum <= 0 || KeyNum > 0xFF) return "&0";
		if ((KeyNum >= 0x41 && KeyNum <= 0x5a) || (KeyNum >= 0x30 && KeyNum <= 0x39))
		{
			char ch[8];
			sprintf(ch, "%c", KeyNum);
			return (const char*)ch;
		}
		int i = 0;
		while (key_alias[i].key != KeyNum && i < 101)
			i++;
		return i == 101 ? "&0" : key_alias[i].name;
	}
	uint16_t GetGameKeyState(uint16_t KeyID)
	{
		uint16_t *gameKeys = (uint16_t *)0xB73458;

		return gameKeys[KeyID];
	}
	void toggleCursor(int iToggle)
	{
		void		*obj = *(void **)(SF->getSAMP()->getSAMPAddr() + 0x21A10C);
		((void(__thiscall *) (void *, int, bool)) (SF->getSAMP()->getSAMPAddr() + 0x9BD30))(obj, iToggle ? 3 : 0, !iToggle);
		if (!iToggle)
		{
			((void(__thiscall *) (void *)) (SF->getSAMP()->getSAMPAddr() + 0x9BC10))(obj);
			SetCursor(LoadCursor(NULL, NULL));
		}
		else SetCursor(LoadCursor(NULL, IDC_ARROW));
	}
};
extern keyboard keyb;