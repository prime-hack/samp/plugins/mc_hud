#pragma comment(lib, "!MenuManager")
#define mcall void __stdcall

#define ibool   __declspec( dllimport ) bool
#define ipbool   __declspec( dllimport ) bool*
#define idword   __declspec( dllimport ) DWORD
#define iint   __declspec( dllimport ) int
#define istring   __declspec( dllimport ) std::string
#define ipchar   __declspec( dllimport ) char*
#define ichar   __declspec( dllimport ) char
#define ivoid   __declspec( dllimport ) void
#define ipoint   __declspec( dllimport ) POINT
#define icolor   __declspec( dllimport ) D3DCOLOR
#define itexture   __declspec( dllimport ) stTextureInfo
#define ifloat   __declspec( dllimport ) float
#define ipfloat   __declspec( dllimport ) float*

ivoid toggleCursor(bool iToggle);
ibool GetCursorState();

//����������� � ����������
idword CreateMenu(char *Title, int X = -1, int Y = -1, int W = 70, int H = 130, D3DCOLOR BackgroundColor = 0xE0000000, D3DCOLOR TextColor = -1, bool present = true, bool Private = false);
ivoid RemoveMenu(DWORD MenuAddr);
//��������� ����
//���������
istring GetMenuTitle(DWORD MenuAddr);
ipoint GetMenuPos(DWORD MenuAddr);
ipoint GetMenuSize(DWORD MenuAddr);
ibool IsMenuPresent(DWORD MenuAddr);
ibool GetMenuPrivateStatus(DWORD MenuAddr);
iint GetScrollBarPosInMenu(DWORD MenuAddr);
itexture *GetMenuTextureBackground(DWORD MenuAddr);
icolor GetMenuBackgroundColor(DWORD MenuAddr);
icolor GetMenuTextColor(DWORD MenuAddr);
ibool isMenuBindedCursor(DWORD MenuAddr);
ipbool GetMenuPresentPointer(DWORD MenuAddr);
//���������
ivoid SetMenuTitle(DWORD MenuAddr, char *Title);
ivoid SetMenuPos(DWORD MenuAddr, POINT XY);
ivoid SetMenuPos(DWORD MenuAddr, int X, int Y);
ivoid SetMenuSize(DWORD MenuAddr, POINT WH);
ivoid SetMenuSize(DWORD MenuAddr, int W, int H);
ivoid SetMenuPrivateStatus(DWORD MenuAddr, bool status);
ivoid SetMenuPresent(DWORD MenuAddr, bool present);
ibool SetScrollBarPosInMenu(DWORD MenuAddr, int pos);
ivoid SetMenuTextureBackground(DWORD MenuAddr, stTextureInfo *texture);
ivoid SetMenuBackgroundColor(DWORD MenuAddr, D3DCOLOR color);
ivoid SetMenuTextColor(DWORD MenuAddr, D3DCOLOR color);
ivoid MenuAutoSizeH(DWORD MenuAddr);
ivoid MenuAutoSizeW(DWORD MenuAddr);
ivoid MenuAutoSize(DWORD MenuAddr);
ivoid MenuBindCursor(DWORD MenuAddr, bool bind);
//��������
//��������
//���� ��������
iint AddListingToMenu(DWORD MenuAddr, int X, int Y, int W, int H, void(_stdcall *pFunc)(int Return, byte Click));
ibool RemoveListingFromMenu(DWORD MenuAddr, int ID);
ibool IsEmptyListingInMenu(DWORD MenuAddr, int ID);
ivoid ResetListingInMenu(DWORD MenuAddr, int ID);
iint GetListingScrollBarPosInMenu(DWORD MenuAddr, int ID);
ipoint GetListingPosInMenu(DWORD MenuAddr, int ID);
ipoint GetListingSizeInMenu(DWORD MenuAddr, int ID);
ivoid SetListingPosInMenu(DWORD MenuAddr, int ID, POINT XY);
ivoid SetListingPosInMenu(DWORD MenuAddr, int ID, int X, int Y);
ivoid SetListingSizeInMenu(DWORD MenuAddr, int ID, POINT WH);
ibool SetListingScrollBarPosInMenu(DWORD MenuAddr, int ID, int pos);
ivoid SetListingSizeInMenu(DWORD MenuAddr, int ID, int X, int Y);
ivoid SetListingReturnInMenu(DWORD MenuAddr, int ID, void(_stdcall *pFunc)(int Return, byte Click));
ivoid AutoScrollListingInMenu(DWORD MenuAddr, int ID);
//����� � ���������
iint AddTextOnListingInMenu(DWORD MenuAddr, int ID, char *text, bool LineBreak = false);
iint AddTumblerOnListingInMenu(DWORD MenuAddr, int ID, char *text, bool *Func, D3DCOLOR Color = 0xFF40FF40);
ibool RemoveTextFromListingInMenu(DWORD MenuAddr, int ID, char *text);
ibool RemoveTextFromListingInMenu(DWORD MenuAddr, int ID, int strID);
iint GetIDbyTextOnListinginMenu(DWORD MenuAddr, int ID, char *text);
istring GetTextByIDOnListingInMenu(DWORD MenuAddr, int ID, int strID);
ivoid SetTextByStrIDOnListingInMenu(DWORD MenuAddr, int ID, int strID, const char *text);
//���� �����
iint AddInputBoxInMenu(DWORD MenuAddr, int X, int Y, int W);
ibool RemoveInputBoxFromMenu(DWORD MenuAddr, int ID);
istring GetTextFromInputBoxInMenu(DWORD MenuAddr, int ID);
ipoint GetInputBoxPosInMenu(DWORD MenuAddr, int ID);
iint GetInputBoxWInMenu(DWORD MenuAddr, int ID);
ibool IsInput(DWORD MenuAddr);
ivoid SetTextInInputBoxInMenu(DWORD MenuAddr, int ID, char *text);
ivoid SetInputBoxPosInMenu(DWORD MenuAddr, int ID, POINT XY);
ivoid SetInputBoxPosInMenu(DWORD MenuAddr, int ID, int X, int Y);
ivoid SetInputBoxWInMenu(DWORD MenuAddr, int ID, int W);
ivoid SetInputBoxCallback(DWORD MenuAddr, int ID, void(_stdcall *pFunc)(std::string text));
ivoid ResetInputBoxMenu(DWORD MenuAddr, int ID);
//������������ ��������
iint AddElementInMenu(DWORD MenuAddr, char *text, int X, int Y, void(_stdcall *pFunc)(int Return));
ibool RemoveElementFromMenu(DWORD MenuAddr, int ID);
istring GetTextFromElementInMenu(DWORD MenuAddr, int ID);
ipoint GetElementPosInMenu(DWORD MenuAddr, int ID);
ivoid SetTextForElementInMenu(DWORD MenuAddr, int ID, char *text);
ivoid SetElementPosInMenu(DWORD MenuAddr, int ID, POINT XY);
ivoid SetElementPosInMenu(DWORD MenuAddr, int ID, int X, int Y);
ivoid SetReturnForElementInMenu(DWORD MenuAddr, int ID, void(_stdcall *pFunc)(int Return));
ibool GetTumblerModeForElementInMenu(DWORD MenuAddr, int ID);
ivoid SetTumblerModeForElementInMenu(DWORD MenuAddr, int ID, bool *Func = NULL, D3DCOLOR Color = 0xFF40FF40);
//�������������
iint AddTumblerInMenu(DWORD MenuAddr, char *text, int X, int Y, bool *Func, D3DCOLOR Color = 0xFF40FF40);
ibool RemoveTumblerFromMenu(DWORD MenuAddr, int ID);
istring GetTextFromTumblerInMenu(DWORD MenuAddr, int ID);
ipoint GetTumblerPosInMenu(DWORD MenuAddr, int ID);
ivoid SetTextForTumblerInMenu(DWORD MenuAddr, int ID, char *text);
ivoid SetTumblerPosInMenu(DWORD MenuAddr, int ID, POINT XY);
ivoid SetTumblerPosInMenu(DWORD MenuAddr, int ID, int X, int Y);
ivoid SetReturnForTumblerInMenu(DWORD MenuAddr, int ID, bool *Func);
icolor GetColorTumblerInMenu(DWORD MenuAddr, int ID);
ivoid SetColorTumblerInMenu(DWORD MenuAddr, int ID, D3DCOLOR Color);
//��������
iint AddSliderToMenu(DWORD MenuAddr, int X, int Y, int W, int H, float* pValue, float min = 0.0f, float max = 1.0f, byte TextAlig = 1, D3DCOLOR color = 0xFF40FF40);
ibool RemoveSliderFromMenu(DWORD MenuAddr, int ID);
ipoint GetPosSliderInMenu(DWORD MenuAddr, int ID);
ipoint GetSizeSliderInMenu(DWORD MenuAddr, int ID);
ifloat GetMinSliderInMenu(DWORD MenuAddr, int ID);
ifloat GetMaxSliderInMenu(DWORD MenuAddr, int ID);
ipfloat GetpValueSliderInMenu(DWORD MenuAddr, int ID);
ivoid SetPosSliderInMenu(DWORD MenuAddr, int ID, int X, int Y);
ivoid SetPosSliderInMenu(DWORD MenuAddr, int ID, POINT XY);
ivoid SetSizeSliderInMenu(DWORD MenuAddr, int ID, int W, int H);
ivoid SetSizeSliderInMenu(DWORD MenuAddr, int ID, POINT WH);
ivoid SetMinSliderInMenu(DWORD MenuAddr, int ID, float min);
ivoid SetMaxSliderInMenu(DWORD MenuAddr, int ID, float max);
ivoid SetpValueSliderInMenu(DWORD MenuAddr, int ID, float *pValue);

class Menu
{
private:
	class cListings
	{
	public:
		cListings(Menu *pMenu)
		{
			this->pMenu = pMenu;
		}
		int AddListing(int X, int Y, int W, int H, void(_stdcall *pFunc)(int Return, byte Click))
		{
			return AddListingToMenu(pMenu->pMenu, X, Y, W, H, pFunc);
		}
		int AddTextOnListing(int ID, const char *text, ...)
		{
			va_list ap;
			char	str[1024];
			va_start(ap, text);
			vsprintf_s(str, text, ap);
			va_end(ap);

			return AddTextOnListingInMenu(pMenu->pMenu, ID, str);
		}
		int AddTumblerOnListing(int ID, char *text, bool *Func, D3DCOLOR Color = 0xFF40FF40)
		{
			return AddTumblerOnListingInMenu(pMenu->pMenu, ID, text, Func, Color);
		}
		int AddTextLBOnListing(int ID, const char *text, ...) //LB - LineBreak
		{
			va_list ap;
			char	str[1024];
			va_start(ap, text);
			vsprintf_s(str, text, ap);
			va_end(ap);

			return AddTextOnListingInMenu(pMenu->pMenu, ID, str, true);
		}
		bool RemoveListing(int ID)
		{
			return RemoveListingFromMenu(pMenu->pMenu, ID);
		}
		bool IsEmpty(int ID)
		{
			return IsEmptyListingInMenu(pMenu->pMenu, ID);
		}
		bool RemoveTextFromListing(int ID, char *text)
		{
			return RemoveTextFromListingInMenu(pMenu->pMenu, ID, text);
		}
		bool RemoveTextFromListing(int ID, int strID)
		{
			return RemoveTextFromListingInMenu(pMenu->pMenu, ID, strID);
		}
		int GetScrollBarPos(int ID)
		{
			return GetListingScrollBarPosInMenu(pMenu->pMenu, ID);
		}
		POINT GetListingPos(int ID)
		{
			return GetListingPosInMenu(pMenu->pMenu, ID);
		}
		POINT GetListingSize(int ID)
		{
			return GetListingSizeInMenu(pMenu->pMenu, ID);
		}
		int GetIDByTextOnListing(int ID, char *text)
		{
			return GetIDbyTextOnListinginMenu(pMenu->pMenu, ID, text);
		}
		std::string GetTextOnListingByID(int ID, int strID)
		{
			return GetTextByIDOnListingInMenu(pMenu->pMenu, ID, strID);
		}
		void SetListingPos(int ID, int X, int Y)
		{
			SetListingPosInMenu(pMenu->pMenu, ID, X, Y);
		}
		void SetListingPos(int ID, POINT XY)
		{
			SetListingPosInMenu(pMenu->pMenu, ID, XY);
		}
		void SetListingSize(int ID, int W, int H)
		{
			SetListingSizeInMenu(pMenu->pMenu, ID, W, H);
		}
		bool SetScrollBarPos(int ID, int pos)
		{
			return SetListingScrollBarPosInMenu(pMenu->pMenu, ID, pos);
		}
		void SetListingSize(int ID, POINT WH)
		{
			SetListingSizeInMenu(pMenu->pMenu, ID, WH);
		}
		void SetListingReturn(int ID, void(_stdcall *pFunc)(int Return, byte Click))
		{
			SetListingReturnInMenu(pMenu->pMenu, ID, pFunc);
		}
		void SetTextOnListingByID(int ID, int strID, const char *text, ...)
		{
			va_list ap;
			char	str[1024];
			va_start(ap, text);
			vsprintf_s(str, text, ap);
			va_end(ap);

			SetTextByStrIDOnListingInMenu(pMenu->pMenu, ID, strID, str);
		}
		void ResetListing(int ID)
		{
			ResetListingInMenu(pMenu->pMenu, ID);
		}		
		void AutoScroll(int ID)
		{
			AutoScrollListingInMenu(pMenu->pMenu, ID);
		}
	private:
		friend class Menu;
		Menu *pMenu;
	};
	class cInputBoxes
	{
	public:
		cInputBoxes(Menu* pMenu)
		{
			this->pMenu = pMenu;
		}
		int AddInputBox(int X, int Y, int W)
		{
			return AddInputBoxInMenu(pMenu->pMenu, X, Y, W);
		}
		bool RemoveInputBox(int ID)
		{
			return RemoveInputBoxFromMenu(pMenu->pMenu, ID);
		}
		std::string GetInputBoxText(int ID)
		{
			return GetTextFromInputBoxInMenu(pMenu->pMenu, ID);
		}
		POINT GetInputBoxPos(int ID)
		{
			return GetInputBoxPosInMenu(pMenu->pMenu, ID);
		}
		int GetInputBoxW(int ID)
		{
			return GetInputBoxWInMenu(pMenu->pMenu, ID);
		}
		bool isInput()
		{
			return IsInput(pMenu->pMenu);
		}
		void SetInputBoxText(int ID, const char *text, ...)
		{
			va_list ap;
			char	str[1024];
			va_start(ap, text);
			vsprintf_s(str, text, ap);
			va_end(ap);

			SetTextInInputBoxInMenu(pMenu->pMenu, ID, str);
		}
		void ResetInputBox(int ID)
		{
			ResetInputBoxMenu(pMenu->pMenu, ID);
		}
		void SetInputBoxPos(int ID, int X, int Y)
		{
			SetInputBoxPosInMenu(pMenu->pMenu, ID, X, Y);
		}
		void SetInputBoxPos(int ID, POINT XY)
		{
			SetInputBoxPosInMenu(pMenu->pMenu, ID, XY);
		}
		void SetInputBoxW(int ID, int W)
		{
			SetInputBoxWInMenu(pMenu->pMenu, ID, W);
		}
		void SetCallback(int ID, void(_stdcall *pFunc)(std::string text))
		{
			SetInputBoxCallback(pMenu->pMenu, ID, pFunc);
		}
	private:
		friend class Menu;
		Menu* pMenu;
	};
	class cElements
	{
	public:
		cElements(Menu* pMenu)
		{
			this->pMenu = pMenu;
		}
		int AddElement(char *text, int X, int Y, void(_stdcall *pFunc)(int Return))
		{
			return AddElementInMenu(pMenu->pMenu, text, X, Y, pFunc);
		}
		int AddElement(int X, int Y, void(_stdcall *pFunc)(int Return), const char *text, ...)
		{
			va_list ap;
			char	str[256];
			va_start(ap, text);
			vsprintf_s(str, text, ap);
			va_end(ap);

			return AddElementInMenu(pMenu->pMenu, str, X, Y, pFunc);
		}
		bool RemoveElement(int ID)
		{
			return RemoveElementFromMenu(pMenu->pMenu, ID);
		}
		std::string GetElementText(int ID)
		{
			return GetTextFromElementInMenu(pMenu->pMenu, ID);
		}
		POINT GetElementPos(int ID)
		{
			return GetElementPosInMenu(pMenu->pMenu, ID);
		}
		void SetElementText(int ID, const char *text, ...)
		{
			va_list ap;
			char	str[256];
			va_start(ap, text);
			vsprintf_s(str, text, ap);
			va_end(ap);

			SetTextForElementInMenu(pMenu->pMenu, ID, str);
		}
		void SetElementPos(int ID, int X, int Y)
		{
			SetElementPosInMenu(pMenu->pMenu, ID, X, Y);
		}
		void SetElementPos(int ID, POINT XY)
		{
			SetElementPosInMenu(pMenu->pMenu, ID, XY);
		}
		void SetElementReturn(int ID, void(_stdcall *pFunc)(int Return))
		{
			SetReturnForElementInMenu(pMenu->pMenu, ID, pFunc);
		}		
		bool GetTumblerMode(int ID)
		{
			return GetTumblerModeForElementInMenu(pMenu->pMenu, ID);
		}
		void SetTumblerMode(int ID, bool *Func = NULL, D3DCOLOR Color = 0xFF40FF40)
		{
			SetTumblerModeForElementInMenu(pMenu->pMenu, ID, Func, Color);
		}
	private:
		friend class Menu;
		Menu* pMenu;
	};
	class cTumblers
	{
	public:
		cTumblers(Menu* pMenu)
		{
			this->pMenu = pMenu;
		}
		int AddTumbler(char *text, int X, int Y, bool *Func, D3DCOLOR Color = 0xFF40FF40)
		{
			return AddTumblerInMenu(pMenu->pMenu, text, X, Y, Func, Color);
		}
		int AddTumbler(int X, int Y, bool *Func, D3DCOLOR Color, const char *text, ...)
		{
			va_list ap;
			char	str[256];
			va_start(ap, text);
			vsprintf_s(str, text, ap);
			va_end(ap);

			return AddTumblerInMenu(pMenu->pMenu, str, X, Y, Func, Color);
		}
		bool RemoveTumbler(int ID)
		{
			return RemoveTumblerFromMenu(pMenu->pMenu, ID);
		}
		std::string GetTumblerText(int ID)
		{
			return GetTextFromTumblerInMenu(pMenu->pMenu, ID);
		}
		POINT GetTumblerPos(int ID)
		{
			return GetTumblerPosInMenu(pMenu->pMenu, ID);
		}
		D3DCOLOR GetTumblerColor(int ID)
		{
			return GetColorTumblerInMenu(pMenu->pMenu, ID);
		}
		void SetTumblerText(int ID, const char *text, ...)
		{
			va_list ap;
			char	str[256];
			va_start(ap, text);
			vsprintf_s(str, text, ap);
			va_end(ap);

			SetTextForTumblerInMenu(pMenu->pMenu, ID, str);
		}
		void SetTumblerPos(int ID, int X, int Y)
		{
			SetTumblerPosInMenu(pMenu->pMenu, ID, X, Y);
		}
		void SetTumblerPos(int ID, POINT XY)
		{
			SetTumblerPosInMenu(pMenu->pMenu, ID, XY);
		}
		void SetTumblerReturn(int ID, bool *Func)
		{
			SetReturnForTumblerInMenu(pMenu->pMenu, ID, Func);
		}
		void SetTumblerColor(int ID, D3DCOLOR Color)
		{
			SetColorTumblerInMenu(pMenu->pMenu, ID, Color);
		}
	private:
		friend class Menu;
		Menu* pMenu;
	};
	class cSliders
	{
	public:
		cSliders(Menu* pMenu)
		{
			this->pMenu = pMenu;
		}
		int AddSlider(int X, int Y, int W, int H, float* pValue, float min = 0.0f, float max = 1.0f, byte TextAlig = 1, D3DCOLOR color = 0xFF40FF40)
		{
			return AddSliderToMenu(pMenu->pMenu, X, Y, W, H, pValue, min, max, TextAlig, color);
		}
		bool RemoveSlider(int ID)
		{
			return RemoveSliderFromMenu(pMenu->pMenu, ID);
		}
		POINT GetPos(int ID)
		{
			return GetPosSliderInMenu(pMenu->pMenu, ID);
		}
		POINT GetSize(int ID)
		{
			return GetSizeSliderInMenu(pMenu->pMenu, ID);
		}
		float GetMin(int ID)
		{
			return GetMinSliderInMenu(pMenu->pMenu, ID);
		}
		float GetMax(int ID)
		{
			return GetMaxSliderInMenu(pMenu->pMenu, ID);
		}
		float* GetpValue(int ID)
		{
			return GetpValueSliderInMenu(pMenu->pMenu, ID);
		}
		void SetPos(int ID, int X, int Y)
		{
			SetPosSliderInMenu(pMenu->pMenu, ID, X, Y);
		}
		void SetPos(int ID, POINT XY)
		{
			SetPosSliderInMenu(pMenu->pMenu, ID, XY);
		}
		void SetSize(int ID, int W, int H)
		{
			SetSizeSliderInMenu(pMenu->pMenu, ID, W, H);
		}
		void SetSize(int ID, POINT WH)
		{
			SetSizeSliderInMenu(pMenu->pMenu, ID, WH);
		}
		void SetMin(int ID, float min)
		{
			SetMinSliderInMenu(pMenu->pMenu, ID, min);
		}
		void SetMax(int ID, float max)
		{
			SetMaxSliderInMenu(pMenu->pMenu, ID, max);
		}
		void SetpValue(int ID, float *pValue)
		{
			SetpValueSliderInMenu(pMenu->pMenu, ID, pValue);
		}
	private:
		friend class Menu;
		Menu* pMenu;
	};
public:
	DWORD pMenu;
	Menu(char *Title, int X = -1, int Y = -1, int W = 70, int H = 130, D3DCOLOR BackgroundColor = 0xE0000000, D3DCOLOR TextColor = -1, bool present = true, bool Private = false)
	{
		pMenu = CreateMenu(Title, X, Y, W, H, BackgroundColor, TextColor, present, Private);
	}
	Menu(DWORD MenuAddr)
	{
		pMenu = MenuAddr;
	}
	~Menu()
	{
		RemoveMenu(pMenu);
	}
	//�������� ����
	cListings *Listings = new cListings(this);
	cInputBoxes *InputBoxes = new cInputBoxes(this);
	cElements *Elements = new cElements(this);
	cTumblers *Tumblers = new cTumblers(this);
	cSliders *Sliders = new cSliders(this);
	//������� ��������� ������
	std::string GetTitle()
	{
		return GetMenuTitle(pMenu);
	}
	POINT GetPosition()
	{
		return GetMenuPos(pMenu);
	}
	POINT GetSize()
	{
		return GetMenuSize(pMenu);
	}
	bool IsPresent()
	{
		return IsMenuPresent(pMenu);
	}
	bool GetPrivateStatus()
	{
		return GetMenuPrivateStatus(pMenu);
	}
	int GetScrollBarPos()
	{
		return GetScrollBarPosInMenu(pMenu);
	}
	stTextureInfo* GetTextureBackground()
	{
		return GetMenuTextureBackground(pMenu);
	}
	D3DCOLOR GetBackgroundColor()
	{
		return GetMenuBackgroundColor(pMenu);
	}
	D3DCOLOR GetTextColor()
	{
		return GetMenuTextColor(pMenu);
	}
	bool isBindedCursor()
	{
		return isMenuBindedCursor(pMenu);
	}
	bool* GetPresentPointer()
	{
		return GetMenuPresentPointer(pMenu);
	}
	//������� ���������� ������
	void SetTitle(char *Title)
	{
		SetMenuTitle(pMenu, Title);
	}
	void SetPosition(int X, int Y)
	{
		SetMenuPos(pMenu, X, Y);
	}
	void SetPosition(POINT XY)
	{
		SetMenuPos(pMenu, XY);
	}
	void SetSize(int W, int H)
	{
		SetMenuSize(pMenu, W, H);
	}
	void SetSize(POINT WH)
	{
		SetMenuSize(pMenu, WH);
	}
	void SetPresent(bool present)
	{
		SetMenuPresent(pMenu, present);
	}
	void SetPrivateStatus(bool status)
	{
		SetMenuPrivateStatus(pMenu, status);
	}
	bool SetScrollBarPos(int pos)
	{
		return SetScrollBarPosInMenu(pMenu, pos);
	}
	void SetTextureBackground(stTextureInfo *texture) //by texture
	{
		SetMenuTextureBackground(pMenu, texture);
	}
	void SetTextureBackground(char *pszFile) //from file
	{
		stTextureInfo *texture = new stTextureInfo;
		texture = SF->getRender()->LoadTextureFromFile(pszFile);
		SetMenuTextureBackground(pMenu, texture);
	}
	void SetTextureBackground(int ResourceID, D3DCOLOR D3DColorKey) //from resource
	{
		stTextureInfo *texture = new stTextureInfo;
		texture =  SF->getRender()->LoadTextureFromResource(ResourceID, D3DColorKey);
		SetMenuTextureBackground(pMenu, texture);
	}
	void SetTextureBackground(LPCVOID pSrcData, UINT SrcDataSize) //from memory
	{
		stTextureInfo *texture = new stTextureInfo;
		texture = SF->getRender()->LoadTextureFromMemory(pSrcData, SrcDataSize);
		SetMenuTextureBackground(pMenu, texture);
	}
	void SetBackgroundColor(D3DCOLOR color)
	{
		SetMenuBackgroundColor(pMenu, color);
	}
	void SetTextColor(D3DCOLOR color)
	{
		SetMenuTextColor(pMenu, color);
	}
	void AutoSizeH()
	{
		MenuAutoSizeH(pMenu);
	}
	void AutoSizeW()
	{
		MenuAutoSizeW(pMenu);
	}
	void AutoSize()
	{
		MenuAutoSize(pMenu);
	}
	void BindCursor(bool bind)
	{
		MenuBindCursor(pMenu, bind);
	}
};
idword GetTopAddr();
idword GetMenuAddrByTitle(char *Title);
ibool MenuLoaded();