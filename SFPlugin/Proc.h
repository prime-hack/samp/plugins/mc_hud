void *memcpy_safe(void *_dest, const void *_src, size_t stLen);
int memset_safe(void *_dest, int c, uint32_t len);
void ScreenFont();
float getSkill(int weaponId);
bool playerIsSpawned();