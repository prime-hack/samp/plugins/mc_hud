#include "main.h"

HUD::HUD()
{
	memcpy_safe(BACKUP_DrawHud, (void*)0x0058EAF0, 6);

	bMC_HUD = false;
	HeartAnim = -1;
	fHealth = 100;

	if (file_exist("SAMPFUNCS\\MC_HUD 2.5\\Slots.png"))
		pSlotsTexture = SF->getRender()->LoadTextureFromFile("SAMPFUNCS\\MC_HUD 2.5\\Slots.png");

	if (file_exist("SAMPFUNCS\\MC_HUD 2.5\\Select.png"))
		pSelectTexture = SF->getRender()->LoadTextureFromFile("SAMPFUNCS\\MC_HUD 2.5\\Select.png");

	if (file_exist("SAMPFUNCS\\MC_HUD 2.5\\0hp.png"))
		p0hpTexture = SF->getRender()->LoadTextureFromFile("SAMPFUNCS\\MC_HUD 2.5\\0hp.png");

	if (file_exist("SAMPFUNCS\\MC_HUD 2.5\\5hp.png"))
		p5hpTexture = SF->getRender()->LoadTextureFromFile("SAMPFUNCS\\MC_HUD 2.5\\5hp.png");

	if (file_exist("SAMPFUNCS\\MC_HUD 2.5\\10hp.png"))
		p10hpTexture = SF->getRender()->LoadTextureFromFile("SAMPFUNCS\\MC_HUD 2.5\\10hp.png");

	if (file_exist("SAMPFUNCS\\MC_HUD 2.5\\0ap.png"))
		p0apTexture = SF->getRender()->LoadTextureFromFile("SAMPFUNCS\\MC_HUD 2.5\\0ap.png");

	if (file_exist("SAMPFUNCS\\MC_HUD 2.5\\5ap.png"))
		p5apTexture = SF->getRender()->LoadTextureFromFile("SAMPFUNCS\\MC_HUD 2.5\\5ap.png");

	if (file_exist("SAMPFUNCS\\MC_HUD 2.5\\10ap.png"))
		p10apTexture = SF->getRender()->LoadTextureFromFile("SAMPFUNCS\\MC_HUD 2.5\\10ap.png");

	if (file_exist("SAMPFUNCS\\MC_HUD 2.5\\power.png"))
		pPowerTexture = SF->getRender()->LoadTextureFromFile("SAMPFUNCS\\MC_HUD 2.5\\power.png");

	if (file_exist("SAMPFUNCS\\MC_HUD 2.5\\power_half.png"))
		pPowerHalfTexture = SF->getRender()->LoadTextureFromFile("SAMPFUNCS\\MC_HUD 2.5\\power_half.png");

	if (file_exist("SAMPFUNCS\\MC_HUD 2.5\\oxygen.png"))
		pOxygenTexture = SF->getRender()->LoadTextureFromFile("SAMPFUNCS\\MC_HUD 2.5\\oxygen.png");

	if (file_exist("SAMPFUNCS\\MC_HUD 2.5\\oxygen_detail.png"))
		pOxygenDetailTexture = SF->getRender()->LoadTextureFromFile("SAMPFUNCS\\MC_HUD 2.5\\oxygen_detail.png");

	char TexturePath[128];
	for (int i = 0; i < 47; ++i)
	{
		if (i != 19 && i != 20 && i != 21)
		{
			sprintf(TexturePath, "SAMPFUNCS\\MC_HUD 2.5\\Weapons\\%d.png", i);
			if (file_exist(TexturePath))
				pWeaponTexture[i] = SF->getRender()->LoadTextureFromFile(TexturePath);
			else pWeaponTexture[i] = NULL;
		}
		else pWeaponTexture[i] = NULL;
	}
}

D3DCOLOR HUD::DynamycColor(float percent)
{
	percent *= 2.55;
	byte Green = percent;
	byte Red = 255.0 - percent;

	return D3DCOLOR_ARGB(0xFF, Red, Green, 0x00);
}

void HUD::Render()
{
	if (!bMC_HUD) return;

	if (!*(byte*)0xBA6769)
		return;

	if (!playerIsSpawned())
		return;

	if (SF->getSAMP()->getInput()->iInputEnabled)
	{
		int ChatInputBoxHight = SF->getSAMP()->getChat()->dwChatboxOffset + 3 * pFont->DrawHeight();
		int HudHight = *(int*)0x00C9C044 - (102 + pFont->DrawHeight());

		if (ChatInputBoxHight > HudHight)
			return;
	}

	if (SF->getGame()->actorInfoGet(ACTOR_SELF, ACTOR_ALIVE) == NULL)
		return;

	DWORD dwScreenTOP = *(int*)0x00C9C040 / 2;
	DWORD dwOffsetHud = dwScreenTOP - 260;

	//Draw weapon
	DWORD dwHightOffset = *(int*)0x00C9C044 - 45;

	if (pSlotsTexture != NULL) //draw slots
		SF->getRender()->DrawTexture(pSlotsTexture, dwOffsetHud, dwHightOffset, 520, 40, 0.0, 0xE0FFFFFF);

	if (pSelectTexture != NULL) //draw select slot
	{
		int ActiveSlot = SF->getGame()->actorInfoGet(ACTOR_SELF, ACTOR_ALIVE)->weapon_slot;
		SF->getRender()->DrawTexture(pSelectTexture, (dwOffsetHud - 2) + ActiveSlot * 40, dwHightOffset - 2, 44, 44, 0.0, 0xE0FFFFFF);
	}

	for (int i = 0; i < 13; ++i) //draw weapon and skills
	{
		int WeaponID = SF->getGame()->actorInfoGet(ACTOR_SELF, ACTOR_ALIVE)->weapon[i].id;
		if (pWeaponTexture[WeaponID] != NULL && (WeaponID != 0 || (WeaponID == i && i == 0)))
		{
			int ammo = SF->getGame()->actorInfoGet(ACTOR_SELF, ACTOR_ALIVE)->weapon[i].ammo;
			if ((WeaponID >= 16 && WeaponID <= 46 && ammo > 0) || WeaponID < 16)
			{
				SF->getRender()->DrawTexture(pWeaponTexture[WeaponID], dwOffsetHud + 6 + i * 40, dwHightOffset + 6, 28, 28, 0.0, 0xEEFFFFFF);

				float Skill = getSkill(WeaponID);
				if (Skill >= 0.0)
				{
					SF->getRender()->DrawBox(dwOffsetHud + 8 + i * 40, dwHightOffset + 30, Skill * 0.24 - 1 > 0.0 ? Skill * 0.24 - 1 : 1, 2, DynamycColor(Skill));
				}
			}
		}
	}

	//Draw money, lvl, wl, ammo
	dwHightOffset -= pFont->DrawHeight() + 7;
	DWORD dwOffsetHudRight = dwScreenTOP + 260;
	char text[128];

	//money
	sprintf(text, "$%08d", *(int*)0x00B7CE50);
	pFont->PrintShadow(text, 0xEEFF4040, dwOffsetHud, dwHightOffset);

	//ammo
	int Slot = SF->getGame()->actorInfoGet(ACTOR_SELF, ACTOR_ALIVE)->weapon_slot;
	int ammo_clip = SF->getGame()->actorInfoGet(ACTOR_SELF, ACTOR_ALIVE)->weapon[Slot].ammo_clip;
	int ammo = SF->getGame()->actorInfoGet(ACTOR_SELF, ACTOR_ALIVE)->weapon[Slot].ammo - ammo_clip;
	sprintf(text, "%d/%d", ammo_clip, ammo);
	pFont->PrintShadow(text, 0xEE4040FF, dwOffsetHudRight - pFont->DrawLength(text), dwHightOffset);

	//lvl and wl
	sprintf(text, "%d {EEFFFFFF}| {EEFFBB40}%d", SF->getSAMP()->getPlayers()->iLocalPlayerScore, *(byte*)0x58DB60);
	pFont->PrintShadow(text, 0xEE40FF40, dwScreenTOP - pFont->DrawLength(text) / 2, dwHightOffset);

	//Draw health
	dwHightOffset -= 24;
	if (p0hpTexture != NULL) //draw 0hp
		SF->getRender()->DrawTexture(p0hpTexture, dwOffsetHud, dwHightOffset, 226, 21, 0.0, 0xD0FFFFFF);

	if (p10hpTexture != NULL) //draw 10hp
	{
		float HP = SF->getGame()->actorInfoGet(ACTOR_SELF, ACTOR_ALIVE)->hitpoints;
		int offset = 0;

		for (; HP >= 10.0; HP -= 10.0, ++offset)
		{
			SF->getRender()->DrawTexture(p10hpTexture, dwOffsetHud + offset * 23, dwHightOffset + (offset == HeartAnim ? -5 : 0), 21, 21, 0.0, 0xEEFFFFFF);
		}
		if (HP >= 7.5)
			SF->getRender()->DrawTexture(p10hpTexture, dwOffsetHud + offset * 23, dwHightOffset + (offset == HeartAnim ? -5 : 0), 21, 21, 0.0, 0xEEFFFFFF);
		else if (p5hpTexture != NULL && HP >= 2.5) //draw 5hp
			SF->getRender()->DrawTexture(p5hpTexture, dwOffsetHud + offset * 23, dwHightOffset + (offset == HeartAnim ? -5 : 0), 12, 21, 0.0, 0xEEFFFFFF);
		if (HeartAnim >= 0 && HeartAnim <= 10 && dwHeartAnim_time < GetTickCount())
		{
			dwHeartAnim_time = GetTickCount() + 75;
			--HeartAnim;
		}
	}

	//Draw power
	if (pPowerTexture != NULL)
	{
		float power = *(float*)0x00B7CDB4 / 31.5;
		if (power > 99.0f) power = 100.0f;
		int offset = 1;

		for (; power >= 10.0; power -= 10.0, ++offset)
		{
			SF->getRender()->DrawTexture(pPowerTexture, dwOffsetHudRight - offset * 23, dwHightOffset, 21, 21, 0.0, 0xFFFFFFFF);
		}
		if (power > 5.0)
			SF->getRender()->DrawTexture(pPowerTexture, dwOffsetHudRight - offset * 23, dwHightOffset, 21, 21, 0.0, 0xFFFFFFFF);
		else if (power >= 0.1) SF->getRender()->DrawTexture(pPowerHalfTexture, dwOffsetHudRight - offset * 23, dwHightOffset, 21, 21, 0.0, 0xFFFFFFFF);
	}

	//Draw armor
	dwHightOffset -= 26;
	float AP = SF->getGame()->actorInfoGet(ACTOR_SELF, ACTOR_ALIVE)->armor;

	if (p0apTexture != NULL && AP > 0.0) //draw 0ap
		SF->getRender()->DrawTexture(p0apTexture, dwOffsetHud, dwHightOffset, 226, 21, 0.0, 0xD0FFFFFF);

	if (p10apTexture != NULL) //draw 10ap
	{
		int offset = 0;

		for (; AP >= 10.0; AP -= 10.0, ++offset)
		{
			SF->getRender()->DrawTexture(p10apTexture, dwOffsetHud + offset * 23, dwHightOffset + (offset == ArmorAnim ? -5 : 0), 21, 21, 0.0, 0xEEFFFFFF);
		}
		if (AP >= 7.5)
			SF->getRender()->DrawTexture(p10apTexture, dwOffsetHud + offset * 23, dwHightOffset + (offset == ArmorAnim ? -5 : 0), 21, 21, 0.0, 0xEEFFFFFF);
		else if (p5apTexture != NULL && AP >= 2.5) //draw 5hp
			SF->getRender()->DrawTexture(p5apTexture, dwOffsetHud + offset * 23, dwHightOffset + (offset == ArmorAnim ? -5 : 0), 12, 21, 0.0, 0xEEFFFFFF);
		if (ArmorAnim >= 0 && ArmorAnim <= 10 && dwArmorAnim_time < GetTickCount())
		{
			dwArmorAnim_time = GetTickCount() + 75;
			--ArmorAnim;
		}
	}

	//Draw oxygen
	if (pOxygenTexture != NULL)
	{
		float oxygen = *(float*)0x00B7CDE0 / 40.0f;
		if (oxygen > 99.0f) oxygen = 100.0f;
		int offset = 1;

		for (; oxygen >= 10.0; oxygen -= 10.0, ++offset)
		{
			SF->getRender()->DrawTexture(pOxygenTexture, dwOffsetHudRight - offset * 23, dwHightOffset, 21, 21, 0.0, 0xFFFFFFFF);
		}
		if (oxygen >= 0.25)
			SF->getRender()->DrawTexture(pOxygenTexture, dwOffsetHudRight - offset * 23, dwHightOffset, 21, 21, 0.0, 0xFFFFFFFF);
		else if (oxygen >= 0.1) SF->getRender()->DrawTexture(pOxygenDetailTexture, dwOffsetHudRight - offset * 23, dwHightOffset, 21, 21, 0.0, 0xFFFFFFFF);
	}
}

void HUD::SetHudState(bool &state)
{
	bMC_HUD = state;

	if (bMC_HUD)
		memcpy_safe((void*)0x0058EAF0, (byte*)"\xC3\x90\x90\x90\x90\x90", 6);
	else memcpy_safe((void*)0x0058EAF0, BACKUP_DrawHud, 6);
}

bool HUD::GetHudState()
{
	return bMC_HUD;
}

void HUD::ApplyDamage()
{
	if (!bMC_HUD) return;

	if (!*(byte*)0xBA6769)
		return;

	if (!playerIsSpawned())
		return;

	if (SF->getGame()->actorInfoGet(ACTOR_SELF, ACTOR_ALIVE) == NULL)
		return;

	float HP = SF->getGame()->actorInfoGet(ACTOR_SELF, ACTOR_ALIVE)->hitpoints;
	if (HP < fHealth)
	{
		dwHeartAnim_time = GetTickCount() + 75;
		HeartAnim = HP / 10.0f;
	}
	fHealth = HP;

	float AP = SF->getGame()->actorInfoGet(ACTOR_SELF, ACTOR_ALIVE)->armor;
	if (AP < fArmor)
	{
		dwArmorAnim_time = GetTickCount() + 75;
		ArmorAnim = HP / 10.0f;
	}
	fArmor = AP;
}

HUD::~HUD()
{
	memcpy_safe((void*)0x0058EAF0, BACKUP_DrawHud, 6);
}
