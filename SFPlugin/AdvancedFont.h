#pragma once
class CAdvancedFont : public CCreateTexture
{
public:
	CAdvancedFont(IDirect3DDevice9 *pDevice, stFontInfo *pFont);
	~CAdvancedFont();

	void Printf(int X, int Y, int W, int H, D3DCOLOR color, const char* szText, float R = 0.0f, bool shadow = false);
	void Printf(int X, int Y, int W, int H, D3DCOLOR color, const char* szText, bool shadow = false, float R = 0.0f);
	void Print(int X, int Y, D3DCOLOR color, float R, bool shadow, const char* szText, ...);
	void Print(int X, int Y, D3DCOLOR color, const char* szText, float R = 0.0f, bool shadow = false);
	void Print(int X, int Y, D3DCOLOR color, const char* szText, bool shadow = false, float R = 0.0f);

	//void ReInit(stFontInfo *pFont);
protected:
	stFontInfo*			pFont;
private:
	DWORD LinarState;
};

